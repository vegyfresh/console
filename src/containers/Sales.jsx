// Packages
import React from 'react';
import { 
    Switch,
    Route,
} from 'react-router-dom';

// Components
import NavList from '../components/NavList';
import Order from '../pages/Orders';

// Styles
import '../assets/styles/containers/Sales.scss';

const Sales = () => {
    return (
        <div className="sales main-conainer">
            <header>
                Header
            </header>
            <nav>
                <NavList />
            </nav>
            <main>  
                <Switch>
                    <Route exact path="/ventas/"><h1>Bienvenido</h1></Route>
                    <Route exact path="/ventas/facturas" component={Order}/>
                    <Route exact path="/ventas/pedidos" component={Order} />
                    <Route exact path="/ventas/pedidos/:key" component={Order} />
                    <Route exact path="/ventas/remisiones" component={Order}/>
                </Switch>
            </main>

        </div>
    );
}

export default Sales;
