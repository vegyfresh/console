import React, { useState } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../actions';
import '../assets/styles/containers/Login.scss';

const Login = (props) => {
    const [ form, setValue ] = useState({});

    const handleInput = (event) => {
        setValue({
            ...form,
            [event.target.name]: event.target.value 
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.signIn(form);
    }

    return (
        <div className="login">
            <div className="login--presentation">
            </div>
            <div className="login--form">
                <div className="form-container">
                    <h1>Bienvenido a ¡VegyFresh!</h1>  
                    <form className="form" onSubmit={handleSubmit}>
                        <div className="input-form">
                            <input type="text" placeholder="Correo" name="email" onChange={handleInput}/>
                        </div>
                        <div className="input-form">
                            <input type="password" placeholder="Contraseña" name="password" onChange={handleInput}/>
                        </div>
                        <div className="input-form input-submit">
                            <input type="submit" className="btn btn-submit" value="Entrar"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

const mapDispatchToProps = {
    signIn
}

export default connect(null, mapDispatchToProps)(Login);