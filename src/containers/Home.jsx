import React from 'react';
import { Link } from 'react-router-dom';
import salesIcon from '../assets/imgs/sales.svg';
import shoppingIcon from '../assets/imgs/shopping.svg';
import '../assets/styles/containers/Home.scss';

const Home = () => (
    <div className="home">
        <div className="home-cointainer">
            <Link to="/ventas" className="home-link">
                <img src={salesIcon} alt="Ventas"/>
            </Link>
            <Link to="/compras" className="home-link">
                <img src={shoppingIcon} alt="Compras"/>
            </Link>
        </div>
    </div>
);

export default Home;