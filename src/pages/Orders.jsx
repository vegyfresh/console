import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { getOrders } from '../request/index';
import Loading from '../components/Loading';
import { BuildTable } from '../components/Table';


const buildDate = (row) => {
    const date = new Date(row.cell.value); 
    return date.toLocaleDateString();
}

const print = (row) => {
    console.log(row.row.values.order_key);
    
    const key = row.row.values.order_key;
    return <Link to={`/ventas/remisiones/${key}`}>Imprimir</Link>;
}

const edit = (row) => {
    const key = row.row.values.order_key
    return <Link to={`/ventas/pedidos/${key}`}>Editar</Link>
}

const Orders = () => {
    const [ orders, setOrders ] = useState([false]);

    const configHeader = [
        { Header: 'Fecha', accessor: 'createdAt', Cell: buildDate },
        { Header: 'Folio', accessor: 'order_key' },
        { Header: 'Nombre del Cliente', accessor: 'client.name'},
        { Header: 'Cliente (No.)', accessor: 'client.client_key'},
        { Header: 'Entrega', accessor: 'deliveryDate', Cell: buildDate },
        { Header: 'Importe Total', accessor: 'totalPrice' },
        { Header: 'Imprimir', accessor: '', Cell: print },
        { Header: 'Editar', accessor: '', Cell: edit }
    ];
    const columns = React.useMemo(() => configHeader, []);


    if(!orders[0]) {
        getOrders()
            .then(body => {
                setOrders(body);
            });
    }

    if (!orders[0]) {
        return (<Loading/>)
    }

    return (
        <div>
            <BuildTable columns={columns} data={orders} />
        </div>
    )
}

export default Orders;