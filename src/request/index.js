import config from '../config/index';
const url = config.url;

// api/orders
export const getOrders = () => {
    return fetch(url + '/orders')
        .then(body => body.json())
        .catch(console.log);
}