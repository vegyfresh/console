// Pakages
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

// Components
import Login from '../containers/Login';
import Home from '../containers/Home';
import Sales from '../containers/Sales';
import Shopping from '../containers/Shopping';


const App = (props) => {
    const isUserLoged = () => {
        if(props.user) {            
            return (
                <>
                <Route exact path='/' component={Login}/>
                <Route component={Login} />
                </>
            );
        } else {
            return (
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/ventas' component={Sales}/>
                    <Route exact path='/ventas/facturas' component={Sales}/>
                    <Route exact path='/ventas/remisiones' component={Sales}/>
                    <Route exact path='/ventas/pedidos' component={Sales}/>
                    <Route exact path='/ventas/pedidos/:key' component={Sales}/>
                    <Route exact path='/compras' component={Shopping}/>
                </Switch>
            )
        }
    }

    return (
        <BrowserRouter>
            {isUserLoged()}  
        </BrowserRouter>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(App);