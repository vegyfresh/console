import React from 'react';
import { Link } from 'react-router-dom';
import creditIcon from '../assets/imgs/credit.svg'
import remissionIcon from '../assets/imgs/remission.svg'
import orderIcon from '../assets/imgs/order.svg'
import '../assets/styles/components/NavList.scss';

const NavList = () => {
    return (
        <div className="nav-list">
            <button className="nav-list-button">
                <p>Documentos</p>
            </button>
            <ul className="nav-list-list">
                <Link to="/ventas/facturas" className="nav-list-item">
                    <img src={creditIcon} alt="Logo de facturas"/>
                    <p>Facturas</p>
                </Link>
                <Link to="/ventas/remisiones" className="nav-list-item">
                    <img src={remissionIcon} alt="Logo de remision"/>
                    <p>Remisión</p>
                </Link>
                <Link to="/ventas/pedidos" className="nav-list-item">
                    <img src={orderIcon} alt="Logo de pedido"/>
                    <p>Pedido</p>
                </Link>
            </ul>
        </div>
    );
}

export default NavList;