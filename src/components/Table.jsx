import React from 'react';
import { useTable } from 'react-table';

export const Table = ({ tableProps, children }) => (
    <table {...tableProps()} className="table">
        {children}
    </table>
);

export const Header = ({ headerGroups }) => (
    <thead>
        {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                    <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                ))}
            </tr>
        ))}
    </thead>
)

export const Body = ({ tableBodyProps, rows, prepareRow }) => (
    <tbody {...tableBodyProps()}>
        {rows.map((row, i) => {
            prepareRow(row);
            return (
                <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    })}
                </tr>
            )
        })}
    </tbody>
)


export const BuildTable = ({ columns, data }) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = useTable({
        columns,
        data
    });

    return (
        <Table tableProps={getTableProps}>
            <Header headerGroups={headerGroups}/>
            <Body 
                tableBodyProps={getTableBodyProps} 
                rows={rows} 
                prepareRow={prepareRow}
            />
        </Table>
    )
}