const ArticleModel = (data) => ({
    key: (data.key) ? data.key : "", 
    name: (data.name) ? data.name : "", 
    avatar: (data.avatar) ? data.avatar : "", 
    price: (data.price) ? data.price : "", 
    state: (data.state) ? data.state : "",
    visibility: (data.visibility) ? data.visibility : "",
    maximum: (data.maximum) ? data.maximum : "",
    minimum: (data.minimum) ? data.minimum : "",
    unity: (data.unity) ? data.unity : "",
    CategoryId: (data.CategoryId) ? data.CategoryId : ""
});

export default ArticleModel;