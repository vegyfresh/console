const pedido = [
    { key: 'A00001', name: 'Papas', unity: 'KG', count: 2, price: 15, import: 30 },
    { key: 'A00002', name: 'Pera', unity: 'KG', count: 1, price: 23, import: 23 },
    { key: 'A00023', name: 'Uva', unity: 'KG', count: 0, price: 0, import: 0 },
    { key: 'A00005', name: 'Ajo', unity: 'KG', count: 0, price: 0, import: 0 },
    { key: 'A00010', name: 'Echalot', unity: 'KG', count: 0, price: 0, import: 0 },
    { key: 'A00004', name: 'Jamaica', unity: 'KG', count: 0, price: 0, import: 0 },
]

export default pedido;